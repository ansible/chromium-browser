# Chromium browser

This role installs `chromium-browser` beta from
[Saikrishna Arcot's PPA](https://launchpad.net/~saiarcot895/+archive/ubuntu/chromium-beta).


## Ungoogled Chromium

I would like this role to configure a completely degoogled Chromium.
This role does not achieve this yet.

[ungoogled-chromium](https://github.com/ungoogled-software/ungoogled-chromium)
does [not support Jammy yet](https://github.com/ungoogled-software/ungoogled-chromium-debian/issues/296).

+ https://github.com/ungoogled-software/ungoogled-chromium-debian
+ https://github.com/NeverDecaf/chromium-web-store


## Chromium and Snap

By default, Chromium browser is installed using Snap on Ubuntu Focal and later.

This can be a source of annoyance on LXC containers
(lots of warnings about `mkdir: cannot create directory '/run/user/1000': Permission denied`,
also see [`archivebox` role](https://codeberg.org/ansible/archivebox)).

But container or not, I prefer to not use Snaps (just a personal preference,
might change down the line, might not).


### Install Chromium without Snap

+ Flatpak? [`org.chromium.Chromium`](https://www.flathub.org/apps/details/org.chromium.Chromium)
+ [Build from source](https://chromium.googlesource.com/chromium/src/+/main/docs/linux/build_instructions.md)
+ [Use Chromium from the Debian "buster" repository](https://askubuntu.com/a/1206153),
  involves a lot of fiddling with apt sources and such
+ [Use Chromium-team `dev` or `beta` PPA](https://launchpad.net/~chromium-team)
+ Install Google Chrome instead (available as single DEB package)
+ [ungoogled-chromium-debian](https://github.com/ungoogled-software/ungoogled-chromium-debian/issues),
  provides [DEB binaries](https://ungoogled-software.github.io/ungoogled-chromium-binaries/releases/debian_unportable/amd64/), but [no support for Jammy yet](https://github.com/ungoogled-software/ungoogled-chromium-debian/issues/296).
+ [Arcot's PPA of Chromium beta](https://launchpad.net/~saiarcot895/+archive/ubuntu/chromium-beta),
  it is not official, but for what it's worth it's referenced from https://code.launchpad.net/chromium-project.
  Not stable, only beta, but supports Ubuntu including Jammy.

+ https://askubuntu.com/questions/1204571/how-to-install-chromium-without-snap
+ https://ahelpme.com/linux/ubuntu/chromium-browser-in-ubuntu-20-04-lts-without-snap-to-use-in-docker-container/


The Ubuntu Focal universe repos contain:
```
$ apt-cache search chromium-browser
chromium-browser - Transitional package - chromium-browser -> chromium snap
chromium-browser-l10n - Transitional package - chromium-browser-l10n -> chromium snap
$ apt-cache madison chromium-browser
chromium-browser | 1:85.0.4183.83-0ubuntu0.20.04.2 | http://archive.ubuntu.com/ubuntu focal-updates/universe amd64 Packages
chromium-browser | 80.0.3987.163-0ubuntu1 | http://archive.ubuntu.com/ubuntu focal/universe amd64 Packages
```

The "Chromium team" PPA seems to be at least semi-official, but unfortunately only offers
dev or beta channels (the stable PPA is discontinued).
Beta is currently at v98.0.4758.54-0ubuntu0.18.04.1, and dev is at v99.0.4818.0-0ubuntu0.18.04.4.
As you can see it's specifically for Bionic, but it's worth trying on Focal.


The Ubuntu Jammy universe repos contain:
```
$ apt-cache search chromium-browser
chromium-browser - Transitional package - chromium-browser -> chromium snap
chromium-browser-l10n - Transitional package - chromium-browser-l10n -> chromium snap
$ apt-cache madison chromium-browser
chromium-browser | 1:85.0.4183.83-0ubuntu2 | http://se.archive.ubuntu.com/ubuntu jammy/universe amd64 Packages
```

So it seems clear that the apt package won't receive priority by the Ubuntu repos,
instead pivoting towards snap.

I don't like snap packages  - they create mounted drives, and if
sandboxing is necessary, I think I prefer flatpak over snap.
Plus, I only have like two or three snaps left, so I think getting rid of all of
them is achievable.




## Refs

+ https://launchpad.net/~saiarcot895/+archive/ubuntu/chromium-beta
+ https://github.com/popey/unsnap/blob/main/applist.csv
+ https://reddit.com/r/Ubuntu/comments/ee2ep0/how_do_i_install_the_not_a_bloody_snap_version_of/
+ https://developer.chrome.com/articles/new-headless
+ https://developer.chrome.com/blog/headless-chrome
