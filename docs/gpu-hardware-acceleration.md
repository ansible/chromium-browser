# Poor hardware acceleration out of the box

Might have become worse by ditching the snap build.

Ways to improve?
https://www.linuxuprising.com/2021/01/how-to-enable-hardware-accelerated.html

Print this log by entering `chrome://gpu` in the browser:

```
Graphics Feature Status
Canvas: Hardware accelerated
Canvas out-of-process rasterization: Disabled
Direct Rendering Display Compositor: Disabled
Compositing: Software only. Hardware acceleration disabled
Multiple Raster Threads: Enabled
OpenGL: Enabled
Rasterization: Hardware accelerated
Raw Draw: Disabled
Skia Renderer: Enabled
Video Decode: Software only. Hardware acceleration disabled
Video Encode: Software only. Hardware acceleration disabled
Vulkan: Disabled
WebGL: Hardware accelerated but at reduced performance
WebGL2: Hardware accelerated but at reduced performance
Driver Bug Workarounds
adjust_src_dst_region_for_blitframebuffer
clear_uniforms_before_first_program_use
enable_webgl_timer_query_extensions
exit_on_context_lost
disabled_extension_GL_KHR_blend_equation_advanced
disabled_extension_GL_KHR_blend_equation_advanced_coherent
Problems Detected
Accelerated video encode has been disabled, either via blocklist, about:flags or the command line.
Disabled Features: video_encode
Accelerated video decode has been disabled, either via blocklist, about:flags or the command line.
Disabled Features: video_decode
Gpu compositing has been disabled, either via blocklist, about:flags or the command line. The browser will fall back to software compositing and hardware acceleration will be unavailable.
Disabled Features: gpu_compositing
Clear uniforms before first program use on all platforms: 124764, 349137
Applied Workarounds: clear_uniforms_before_first_program_use
Disable KHR_blend_equation_advanced until cc shaders are updated: 661715
Applied Workarounds: disable(GL_KHR_blend_equation_advanced), disable(GL_KHR_blend_equation_advanced_coherent)
Expose WebGL's disjoint_timer_query extensions on platforms with site isolation: 808744, 870491
Applied Workarounds: enable_webgl_timer_query_extensions
Some drivers can't recover after OUT_OF_MEM and context lost: 893177
Applied Workarounds: exit_on_context_lost
adjust src/dst region if blitting pixels outside framebuffer on Linux NVIDIA: 830046
Applied Workarounds: adjust_src_dst_region_for_blitframebuffer
ANGLE Features
allow_compressed_formats (Frontend workarounds): Enabled: true
Allow compressed formats
disable_anisotropic_filtering (Frontend workarounds): Disabled
Disable support for anisotropic filtering
disable_program_binary (Frontend features) anglebug:5007: Disabled
Disable support for GL_OES_get_program_binary
disable_program_caching_for_transform_feedback (Frontend workarounds): Disabled
On some GPUs, program binaries don't contain transform feedback varyings
enableCompressingPipelineCacheInThreadPool (Frontend workarounds) anglebug:4722: Disabled: false
Enable compressing pipeline cache in thread pool.
enableProgramBinaryForCapture (Frontend features) anglebug:5658: Disabled
Even if FrameCapture is enabled, enable GL_OES_get_program_binary
enable_capture_limits (Frontend features) anglebug:5750: Disabled
Set the context limits like frame capturing was enabled
forceInitShaderVariables (Frontend features): Disabled
Force-enable shader variable initialization
forceRobustResourceInit (Frontend features) anglebug:6041: Disabled
Force-enable robust resource init
lose_context_on_out_of_memory (Frontend workarounds): Enabled: true
Some users rely on a lost context notification if a GL_OUT_OF_MEMORY error occurs
scalarize_vec_and_mat_constructor_args (Frontend workarounds) 1165751: Disabled: false
Always rewrite vec/mat constructors to be consistent
allocateNonZeroMemory (Vulkan features) anglebug:4384: Disabled: false
Fill new allocations with non-zero values to flush out errors.
allowGenerateMipmapWithCompute (Vulkan features) anglebug:4551: Enabled: supportsSubgroupQuadOpsInComputeShader && mSubgroupExtendedTypesFeatures.shaderSubgroupExtendedTypes && maxComputeWorkGroupInvocations >= 256 && ((isAMD && !IsWindows()) || isNvidia || isSamsung)
Use the compute path to generate mipmaps on devices that meet the minimum requirements, and the performance is better.
asyncCommandQueue (Vulkan features) anglebug:4324: Disabled: false
Use CommandQueue worker thread to dispatch work to GPU.
basicGLLineRasterization (Vulkan features): Disabled
Enable the use of pixel shader patching to implement OpenGL basic line rasterization rules
bindEmptyForUnusedDescriptorSets (Vulkan workarounds) anglebug:2727: Disabled: IsAndroid() && isQualcomm
Gaps in bound descriptor set indices causes the post-gap sets to misbehave
bottomLeftOriginPresentRegionRectangles (Vulkan workarounds): Disabled: IsAndroid()
On some platforms present region rectangles are expected to have a bottom-left origin, instead of top-left origin as from spec
bresenhamLineRasterization (Vulkan features): Enabled: true
Enable Bresenham line rasterization via VK_EXT_line_rasterization extension
clampPointSize (Vulkan workarounds) anglebug:2970: Disabled: isNvidia && nvidiaVersion.major < uint32_t(IsWindows() ? 430 : 421)
The point size range reported from the API is inconsistent with the actual behavior
compress_vertex_data (Vulkan workarounds): Disabled: false
Compress vertex data to smaller data types when possible. Using this feature makes ANGLE non-conformant.
createPipelineDuringLink (Vulkan features) anglebug:7046: Disabled
Create pipeline with default state during glLinkProgram
deferFlushUntilEndRenderPass (Vulkan workarounds) https://issuetracker.google.com/issues/166475273: Enabled: !isQualcomm
Allow glFlush to be deferred until renderpass ends
depth_clamping (Vulkan workarounds) anglebug:3970: Enabled: isNvidia && mPhysicalDeviceFeatures.depthClamp && ExtensionFound("VK_EXT_depth_clip_enable", deviceExtensionNames) && (!IsLinux() || nvidiaVersion.major > 418u)
The depth value is not clamped to [0,1] for floating point depth buffers.
disableFifoPresentMode (Vulkan workarounds) anglebug:3153: Disabled: IsLinux() && isIntel
VK_PRESENT_MODE_FIFO_KHR causes random timeouts
disableFlippingBlitWithCommand (Vulkan workarounds) anglebug:3498: Disabled: IsAndroid() && isQualcomm
vkCmdBlitImage with flipped coordinates blits incorrectly.
disallowSeamfulCubeMapEmulation (Vulkan workarounds) anglebug:3243: Disabled: IsWindows() && isAMD
Seamful cube map emulation misbehaves on some drivers, so it's disallowed
emulateDithering (Vulkan features) anglebug:6755: Disabled: IsAndroid()
Emulate OpenGL dithering
emulateR32fImageAtomicExchange (Vulkan workarounds) anglebug:5535: Enabled: true
Emulate r32f images with r32ui to support imageAtomicExchange.
emulateTransformFeedback (Vulkan features) anglebug:3205: Disabled: (!mFeatures.supportsTransformFeedbackExtension.enabled && mPhysicalDeviceFeatures.vertexPipelineStoresAndAtomics == 1U)
Emulate transform feedback as the VK_EXT_transform_feedback is not present.
emulatedPrerotation180 (Vulkan features) anglebug:4901: Disabled
Emulate 180-degree prerotation.
emulatedPrerotation270 (Vulkan features) anglebug:4901: Disabled
Emulate 270-degree prerotation.
emulatedPrerotation90 (Vulkan features) anglebug:4901: Disabled
Emulate 90-degree prerotation.
enableMultisampledRenderToTexture (Vulkan workarounds) anglebug:4937: Enabled: mFeatures.supportsMultisampledRenderToSingleSampled.enabled || (supportsIndependentDepthStencilResolve && !isSwiftShader && !(IsWindows() && (isIntel || isAMD)))
Expose EXT_multisampled_render_to_texture
enablePreRotateSurfaces (Vulkan features) anglebug:3502: Disabled: IsAndroid() && supportsNegativeViewport
Enable Android pre-rotation for landscape applications
enablePrecisionQualifiers (Vulkan features) anglebug:3078: Enabled: !(IsPixel2(mPhysicalDeviceProperties.vendorID, mPhysicalDeviceProperties.deviceID) && (mPhysicalDeviceProperties.driverVersion < kPixel2DriverWithRelaxedPrecision)) && !IsPixel4(mPhysicalDeviceProperties.vendorID, mPhysicalDeviceProperties.deviceID)
Enable precision qualifiers in shaders
exposeNonConformantExtensionsAndVersions (Vulkan workarounds) anglebug:5375: Disabled: kExposeNonConformantExtensionsAndVersions
Expose GLES versions and extensions that are not conformant.
forceD16TexFilter (Vulkan workarounds) anglebug:3452: Disabled: IsAndroid() && isQualcomm
VK_FORMAT_D16_UNORM does not support VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT, which prevents OES_depth_texture from being supported.
forceDriverUniformOverSpecConst (Vulkan workarounds) http://issuetracker.google.com/173636783: Disabled: isQualcomm && mPhysicalDeviceProperties.driverVersion < kPixel4DriverWithWorkingSpecConstSupport
Forces using driver uniforms instead of specialization constants.
forceFallbackFormat (Vulkan workarounds): Disabled
Force a fallback format for angle_end2end_tests
forceFragmentShaderPrecisionHighpToMediump (Vulkan workarounds) https://issuetracker.google.com/184850002: Disabled: false
Forces highp precision in fragment shader to mediump.
forceMaxUniformBufferSize16KB (Vulkan workarounds) https://issuetracker.google.com/161903006: Disabled: isQualcomm && isAdreno540
Force max uniform buffer size to 16K on some device due to bug
forceNearestMipFiltering (Vulkan workarounds): Disabled: false
Force nearest mip filtering when sampling.
forceSubmitImmutableTextureUpdates (Unknown) anglebug:6929: Disabled
Force submit updates to immutable textures
force_nearest_filtering (Vulkan workarounds): Disabled: false
Force nearest filtering when sampling.
force_texture_lod_offset_1 (Vulkan workarounds): Disabled: false
Increase the minimum texture level-of-detail by 1 when sampling.
force_texture_lod_offset_2 (Vulkan workarounds): Disabled: false
Increase the minimum texture level-of-detail by 2 when sampling.
force_texture_lod_offset_3 (Vulkan workarounds): Disabled: false
Increase the minimum texture level-of-detail by 3 when sampling.
force_texture_lod_offset_4 (Vulkan workarounds): Disabled: false
Increase the minimum texture level-of-detail by 4 when sampling.
generateSPIRVThroughGlslang (Vulkan features) anglebug:4889: Disabled: kUseSpirvGenThroughGlslang
Translate SPIR-V through glslang.
logMemoryReportCallbacks (Vulkan features): Disabled: false
Log each callback from VK_EXT_device_memory_report
logMemoryReportStats (Vulkan features): Disabled: false
Log stats from VK_EXT_device_memory_report each swap
overrideSurfaceFormatRGB8toRGBA8 (Vulkan workarounds) anglebug:6651: Enabled: true
Override surface format GL_RGB8 to GL_RGBA8
padBuffersToMaxVertexAttribStride (Vulkan workarounds) anglebug:4428: Disabled: isAMD || isSamsung
Vulkan considers vertex attribute accesses to count up to the last multiple of the stride. This additional access supports AMD's robust buffer access implementation. AMDVLK in particular will return incorrect values when the vertex access extends into the range that would be the stride padding and the buffer is too small. This workaround limits GL_MAX_VERTEX_ATTRIB_STRIDE to a maximum value and pads up every buffer allocation size to be a multiple of the maximum stride.
perFrameWindowSizeQuery (Vulkan workarounds) anglebug:3623: Disabled: isIntel || (IsWindows() && isAMD) || IsFuchsia() || isSamsung
Vulkan swapchain is not returning VK_ERROR_OUT_OF_DATE when window resizing
persistentlyMappedBuffers (Vulkan features) anglebug:2162: Enabled: true
Persistently map buffer memory to reduce map/unmap IOCTL overhead.
preferAggregateBarrierCalls (Vulkan workarounds) anglebug:4633: Enabled: isNvidia || isAMD || isIntel || isSwiftShader || isSamsung
Single barrier call is preferred over multiple calls with fine grained pipeline stage dependency information
preferCPUForBufferSubData (Vulkan features) http://issuetracker.google.com/200067929: Disabled: isARM
Prefer use CPU to do bufferSubData instead of staged update.
preferDrawClearOverVkCmdClearAttachments (Vulkan workarounds) https://issuetracker.google.com/166809097: Disabled: IsPixel2(mPhysicalDeviceProperties.vendorID, mPhysicalDeviceProperties.deviceID)
On some hardware, clear using a draw call instead of vkCmdClearAttachments in the middle of render pass due to bugs
preferSubmitAtFBOBoundary (Vulkan workarounds) https://issuetracker.google.com/187425444: Disabled: isARM
Submit commands to driver at each FBO boundary for performance improvements.
provokingVertex (Vulkan features): Enabled: true
Enable provoking vertex mode via VK_EXT_provoking_vertex extension
retainSpirvDebugInfo (Vulkan features) anglebug:5901: Disabled: getEnableValidationLayers()
Retain debug info in SPIR-V blob.
shadowBuffers (Vulkan features) anglebug:4339: Disabled: false
Allocate a shadow buffer for GL buffer objects to reduce glMap* latency.
supportsAndroidHardwareBuffer (Vulkan features): Disabled
VkDevice supports the VK_ANDROID_external_memory_android_hardware_buffer extension
supportsAndroidNativeFenceSync (Vulkan features) anglebug:2517: Disabled
VkDevice supports the EGL_ANDROID_native_fence_sync extension
supportsBlendOperationAdvanced (Vulkan features) anglebug:3586: Enabled: mBlendOperationAdvancedFeatures.advancedBlendCoherentOperations == 1U
VkDevice supports VK_EXT_blend_operation_advanced extension.
supportsCustomBorderColor (Vulkan features) anglebug:3577: Enabled: mCustomBorderColorFeatures.customBorderColors == 1U && mCustomBorderColorFeatures.customBorderColorWithoutFormat == 1U && !isSwiftShader
VkDevice supports the VK_EXT_custom_border_color extension
supportsDepthClipControl (Vulkan features) anglebug:5421: Enabled: mDepthClipControlFeatures.depthClipControl == 1U
VkDevice supports VK_EXT_depth_clip_control extension.
supportsDepthStencilResolve (Vulkan features) anglebug:4836: Enabled: mFeatures.supportsRenderpass2.enabled && mDepthStencilResolveProperties.supportedDepthResolveModes != 0
VkDevice supports the VK_KHR_depth_stencil_resolve extension with the independentResolveNone feature
supportsExternalFenceCapabilities (Vulkan features): Enabled: true
VkInstance supports the VK_KHR_external_fence_capabilities extension
supportsExternalFenceFd (Vulkan features) anglebug:2517: Enabled: ExtensionFound("VK_KHR_external_fence_fd", deviceExtensionNames)
VkDevice supports the VK_KHR_external_fence_fd extension
supportsExternalMemoryDmaBufAndModifiers (Vulkan features) anglebug:6248: Disabled: ExtensionFound("VK_EXT_external_memory_dma_buf", deviceExtensionNames) && ExtensionFound("VK_EXT_image_drm_format_modifier", deviceExtensionNames)
VkDevice supports the VK_EXT_external_memory_dma_buf and VK_EXT_image_drm_format_modifier extensions
supportsExternalMemoryFd (Vulkan features): Enabled: ExtensionFound("VK_KHR_external_memory_fd", deviceExtensionNames)
VkDevice supports the VK_KHR_external_memory_fd extension
supportsExternalMemoryFuchsia (Vulkan features): Disabled: ExtensionFound("VK_FUCHSIA_external_memory", deviceExtensionNames)
VkDevice supports the VK_FUCHSIA_external_memory extension
supportsExternalMemoryHost (Vulkan features): Enabled: ExtensionFound("VK_EXT_external_memory_host", deviceExtensionNames)
VkDevice supports the VK_EXT_external_memory_host extension
supportsExternalSemaphoreCapabilities (Vulkan features): Enabled: true
VkInstance supports the VK_KHR_external_semaphore_capabilities extension
supportsExternalSemaphoreFd (Vulkan features): Enabled: ExtensionFound("VK_KHR_external_semaphore_fd", deviceExtensionNames)
VkDevice supports the VK_KHR_external_semaphore_fd extension
supportsExternalSemaphoreFuchsia (Vulkan features): Disabled: ExtensionFound("VK_FUCHSIA_external_semaphore", deviceExtensionNames)
VkDevice supports the VK_FUCHSIA_external_semaphore extension
supportsFilteringPrecision (Vulkan features): Disabled: ExtensionFound("VK_GOOGLE_sampler_filtering_precision", deviceExtensionNames)
VkDevice supports the VK_GOOGLE_sampler_filtering_precision extension
supportsGGPFrameToken (Vulkan features): Disabled
VkDevice supports the VK_GGP_frame_token extension
supportsGeometryStreamsCapability (Vulkan features) anglebug:3206: Enabled: mTransformFeedbackFeatures.geometryStreams == 1U
Implementation supports the GeometryStreams SPIR-V capability.
supportsHostQueryReset (Vulkan features) anglebug:6692: Enabled: (mHostQueryResetFeatures.hostQueryReset == 1U)
VkDevice supports VK_EXT_host_query_reset extension
supportsImageCubeArray (Vulkan features) anglebug:3584: Enabled: mPhysicalDeviceFeatures.imageCubeArray == 1U
VkDevice supports the imageCubeArray feature properly
supportsImageFormatList (Vulkan features) anglebug:5281: Enabled: ExtensionFound("VK_KHR_image_format_list", deviceExtensionNames)
Enable VK_IMAGE_CREATE_MUTABLE_FORMAT_BIT by default for ICDs that support VK_KHR_image_format_list
supportsIncrementalPresent (Vulkan features): Disabled: ExtensionFound("VK_KHR_incremental_present", deviceExtensionNames)
VkDevice supports the VK_KHR_incremental_present extension
supportsIndexTypeUint8 (Vulkan features) anglebug:4405: Enabled: mIndexTypeUint8Features.indexTypeUint8 == 1U
VkDevice supports the VK_EXT_index_type_uint8 extension
supportsLockSurfaceExtension (Vulkan features): Disabled: IsAndroid()
Surface supports the EGL_KHR_lock_surface3 extension
supportsMultiDrawIndirect (Vulkan features) anglebug:6439: Enabled: mPhysicalDeviceFeatures.multiDrawIndirect == 1U
VkDevice supports the multiDrawIndirect extension
supportsMultisampledRenderToSingleSampled (Vulkan features) anglebug:4836: Disabled: mFeatures.supportsRenderpass2.enabled && mFeatures.supportsDepthStencilResolve.enabled && mMultisampledRenderToSingleSampledFeatures.multisampledRenderToSingleSampled == 1U
VkDevice supports the VK_EXT_multisampled_render_to_single_sampled extension
supportsMultiview (Vulkan features) anglebug:6048: Enabled: mMultiviewFeatures.multiview == 1U
VkDevice supports the VK_KHR_multiview extension
supportsNegativeViewport (Vulkan features): Enabled: supportsNegativeViewport
The driver supports inverting the viewport with a negative height.
supportsPipelineStatisticsQuery (Vulkan features) anglebug:5430: Enabled: mPhysicalDeviceFeatures.pipelineStatisticsQuery == 1U
VkDevice supports the pipelineStatisticsQuery feature
supportsProtectedMemory (Vulkan features) anglebug:3965: Disabled: (mProtectedMemoryFeatures.protectedMemory == 1U) && !isARM
VkDevice supports protected memory
supportsRenderPassLoadStoreOpNone (Vulkan features) anglebug:5371: Enabled: ExtensionFound("VK_EXT_load_store_op_none", deviceExtensionNames)
VkDevice supports VK_EXT_load_store_op_none extension.
supportsRenderPassStoreOpNoneQCOM (Vulkan features) anglebug:5055: Disabled: !mFeatures.supportsRenderPassLoadStoreOpNone.enabled && ExtensionFound("VK_QCOM_render_pass_store_ops", deviceExtensionNames)
VkDevice supports VK_QCOM_render_pass_store_ops extension.
supportsRenderpass2 (Vulkan features): Enabled: ExtensionFound("VK_KHR_create_renderpass2", deviceExtensionNames)
VkDevice supports the VK_KHR_create_renderpass2 extension
supportsShaderFloat16 (Vulkan features) anglebug:4551: Disabled: mShaderFloat16Int8Features.shaderFloat16 == 1U
VkDevice supports the VK_KHR_shader_float16_int8 extension and has the shaderFloat16 feature
supportsShaderFramebufferFetch (Vulkan features): Disabled: IsAndroid() && isARM
Whether the Vulkan backend supports coherent framebuffer fetch
supportsShaderFramebufferFetchNonCoherent (Vulkan features): Disabled: IsAndroid() && !(isARM || isQualcomm)
Whether the Vulkan backend supports non-coherent framebuffer fetch
supportsShaderStencilExport (Vulkan features): Disabled: ExtensionFound("VK_EXT_shader_stencil_export", deviceExtensionNames)
VkDevice supports the VK_EXT_shader_stencil_export extension
supportsSharedPresentableImageExtension (Vulkan features): Disabled
VkSurface supports the VK_KHR_shared_presentable_images extension
supportsSurfaceCapabilities2Extension (Vulkan features): Enabled: true
VkInstance supports the VK_KHR_get_surface_capabilities2 extension
supportsSurfaceProtectedCapabilitiesExtension (Vulkan features): Enabled: true
VkInstance supports the VK_KHR_surface_protected_capabilities extension
supportsSurfaceProtectedSwapchains (Vulkan features): Disabled: IsAndroid()
VkSurface supportsProtected for protected swapchains
supportsSurfacelessQueryExtension (Vulkan features): Disabled
VkInstance supports the VK_GOOGLE_surfaceless_query extension
supportsTransformFeedbackExtension (Vulkan features) anglebug:3206: Enabled: mTransformFeedbackFeatures.transformFeedback == 1U
Transform feedback uses the VK_EXT_transform_feedback extension.
supportsYUVSamplerConversion (Vulkan features): Enabled: mSamplerYcbcrConversionFeatures.samplerYcbcrConversion != 0U
VkDevice supports the VK_KHR_sampler_ycbcr_conversion extension
swapbuffersOnFlushOrFinishWithSingleBuffer (Vulkan features) anglebug:6878: Disabled: IsAndroid()
Bypass deferredFlush with calling swapbuffers on flush or finish when in Shared Present mode
useMultipleDescriptorsForExternalFormats (Vulkan workarounds) anglebug:6141: Enabled: true
Return a default descriptor count for external formats.
waitIdleBeforeSwapchainRecreation (Vulkan workarounds) anglebug:5061: Disabled: IsAndroid() && isARM
Before passing an oldSwapchain to VkSwapchainCreateInfoKHR, wait for queue to be idle. Works around a bug on platforms which destroy oldSwapchain in vkCreateSwapchainKHR.
DAWN Info

<Discrete GPU> Vulkan backend - Quadro P620
[Default Toggle Names]
lazy_clear_resource_on_first_use: https://crbug.com/dawn/145: Clears resource to zero on first usage. This initializes the resource so that no dirty bits from recycled memory is present in the new resource.
use_temporary_buffer_in_texture_to_texture_copy: https://crbug.com/dawn/42: Split texture-to-texture copy into two copies: copy from source texture into a temporary buffer, and copy from the temporary buffer into the destination texture when copying between compressed textures that don't have block-aligned sizes. This workaround is enabled by default on all Vulkan drivers to solve an issue in the Vulkan SPEC about the texture-to-texture copies with compressed formats. See #1005 (https://github.com/KhronosGroup/Vulkan-Docs/issues/1005) for more details.
vulkan_use_d32s8: https://crbug.com/dawn/286: Vulkan mandates support of either D32_FLOAT_S8 or D24_UNORM_S8. When available the backend will use D32S8 (toggle to on) but setting the toggle to off will make it use the D24S8 format when possible.
disallow_unsafe_apis: http://crbug.com/1138528: Produces validation errors on API entry points or parameter combinations that aren't considered secure yet.
use_vulkan_zero_initialize_workgroup_memory_extension: https://crbug.com/dawn/1302: Initialize workgroup memory with OpConstantNull on Vulkan when the Vulkan extension VK_KHR_zero_initialize_workgroup_memory is supported.
[WebGPU Forced Toggles - enabled]
disallow_spirv: https://crbug.com/1214923: Disallow usage of SPIR-V completely so that only WGSL is used for shader modules.This is useful to prevent a Chromium renderer process from successfully sendingSPIR-V code to be compiled in the GPU process.
[Supported Features]
texture-compression-bc
pipeline-statistics-query
timestamp-query
depth-clamping
depth24unorm-stencil8
depth32float-stencil8
dawn-internal-usages
dawn-native

<CPU> Vulkan backend - llvmpipe (LLVM 13.0.1, 256 bits)
[Default Toggle Names]
lazy_clear_resource_on_first_use: https://crbug.com/dawn/145: Clears resource to zero on first usage. This initializes the resource so that no dirty bits from recycled memory is present in the new resource.
use_temporary_buffer_in_texture_to_texture_copy: https://crbug.com/dawn/42: Split texture-to-texture copy into two copies: copy from source texture into a temporary buffer, and copy from the temporary buffer into the destination texture when copying between compressed textures that don't have block-aligned sizes. This workaround is enabled by default on all Vulkan drivers to solve an issue in the Vulkan SPEC about the texture-to-texture copies with compressed formats. See #1005 (https://github.com/KhronosGroup/Vulkan-Docs/issues/1005) for more details.
vulkan_use_d32s8: https://crbug.com/dawn/286: Vulkan mandates support of either D32_FLOAT_S8 or D24_UNORM_S8. When available the backend will use D32S8 (toggle to on) but setting the toggle to off will make it use the D24S8 format when possible.
disallow_unsafe_apis: http://crbug.com/1138528: Produces validation errors on API entry points or parameter combinations that aren't considered secure yet.
[WebGPU Forced Toggles - enabled]
disallow_spirv: https://crbug.com/1214923: Disallow usage of SPIR-V completely so that only WGSL is used for shader modules.This is useful to prevent a Chromium renderer process from successfully sendingSPIR-V code to be compiled in the GPU process.
[Supported Features]
texture-compression-bc
pipeline-statistics-query
timestamp-query
depth-clamping
depth24unorm-stencil8
depth32float-stencil8
dawn-internal-usages
dawn-native

<CPU> Vulkan backend - SwiftShader Device (Subzero)
[Default Toggle Names]
lazy_clear_resource_on_first_use: https://crbug.com/dawn/145: Clears resource to zero on first usage. This initializes the resource so that no dirty bits from recycled memory is present in the new resource.
use_temporary_buffer_in_texture_to_texture_copy: https://crbug.com/dawn/42: Split texture-to-texture copy into two copies: copy from source texture into a temporary buffer, and copy from the temporary buffer into the destination texture when copying between compressed textures that don't have block-aligned sizes. This workaround is enabled by default on all Vulkan drivers to solve an issue in the Vulkan SPEC about the texture-to-texture copies with compressed formats. See #1005 (https://github.com/KhronosGroup/Vulkan-Docs/issues/1005) for more details.
vulkan_use_d32s8: https://crbug.com/dawn/286: Vulkan mandates support of either D32_FLOAT_S8 or D24_UNORM_S8. When available the backend will use D32S8 (toggle to on) but setting the toggle to off will make it use the D24S8 format when possible.
disallow_unsafe_apis: http://crbug.com/1138528: Produces validation errors on API entry points or parameter combinations that aren't considered secure yet.
use_vulkan_zero_initialize_workgroup_memory_extension: https://crbug.com/dawn/1302: Initialize workgroup memory with OpConstantNull on Vulkan when the Vulkan extension VK_KHR_zero_initialize_workgroup_memory is supported.
[WebGPU Forced Toggles - enabled]
disallow_spirv: https://crbug.com/1214923: Disallow usage of SPIR-V completely so that only WGSL is used for shader modules.This is useful to prevent a Chromium renderer process from successfully sendingSPIR-V code to be compiled in the GPU process.
[Supported Features]
texture-compression-bc
texture-compression-etc2
texture-compression-astc
timestamp-query
depth-clamping
depth32float-stencil8
dawn-internal-usages
dawn-native
Version Information
Data exported	2022-08-20T18:06:56.161Z
Chrome version	Chrome/101.0.4951.15
Operating system	Linux 5.15.0-43-generic
Software rendering list URL	https://chromium.googlesource.com/chromium/src/+/0dcf182bd3881ac6567fb51e86a17ba341d19475/gpu/config/software_rendering_list.json
Driver bug list URL	https://chromium.googlesource.com/chromium/src/+/0dcf182bd3881ac6567fb51e86a17ba341d19475/gpu/config/gpu_driver_bug_list.json
ANGLE commit id	unknown hash
2D graphics backend	Skia/101 28d3788a1118d48d5eb8bc55b25fa2005aaeeea0
Command Line	/usr/lib/chromium-browser/chromium-browser --enable-pinch --new-window --app=https://emby.chepec.se --enable-crashpad --flag-switches-begin --flag-switches-end
Driver Information
Initialization time	232
In-process GPU	false
Passthrough Command Decoder	true
Sandboxed	false
GPU0	VENDOR= 0x10de [Google Inc. (NVIDIA)], DEVICE=0x1cb6 [ANGLE (NVIDIA, Vulkan 1.3.194 (NVIDIA Quadro P620 (0x00001CB6)), NVIDIA-510.85.2.0)] *ACTIVE*
Optimus	false
AMD switchable	false
Driver vendor	Nvidia
Driver version	510.85.02
GPU CUDA compute capability major version	0
Pixel shader version	1.00
Vertex shader version	1.00
Max. MSAA samples	8
Machine model name	
Machine model version	
GL_VENDOR	Google Inc. (NVIDIA)
GL_RENDERER	ANGLE (NVIDIA, Vulkan 1.3.194 (NVIDIA Quadro P620 (0x00001CB6)), NVIDIA-510.85.2.0)
GL_VERSION	OpenGL ES 2.0.0 (ANGLE 2.1.0 git hash: unknown hash)
GL_EXTENSIONS	GL_AMD_performance_monitor GL_ANGLE_base_vertex_base_instance GL_ANGLE_base_vertex_base_instance_shader_builtin GL_ANGLE_client_arrays GL_ANGLE_depth_texture GL_ANGLE_framebuffer_blit GL_ANGLE_framebuffer_multisample GL_ANGLE_get_image GL_ANGLE_get_serialized_context_string GL_ANGLE_get_tex_level_parameter GL_ANGLE_instanced_arrays GL_ANGLE_memory_object_flags GL_ANGLE_memory_size GL_ANGLE_multi_draw GL_ANGLE_program_cache_control GL_ANGLE_relaxed_vertex_attribute_type GL_ANGLE_request_extension GL_ANGLE_rgbx_internal_format GL_ANGLE_robust_client_memory GL_ANGLE_robust_fragment_shader_output GL_ANGLE_texture_compression_dxt3 GL_ANGLE_texture_compression_dxt5 GL_ANGLE_texture_usage GL_ANGLE_vulkan_image GL_APPLE_clip_distance GL_CHROMIUM_bind_generates_resource GL_CHROMIUM_bind_uniform_location GL_CHROMIUM_color_buffer_float_rgb GL_CHROMIUM_color_buffer_float_rgba GL_CHROMIUM_copy_compressed_texture GL_CHROMIUM_copy_texture GL_CHROMIUM_lose_context GL_EXT_EGL_image_external_wrap_modes GL_EXT_blend_func_extended GL_EXT_blend_minmax GL_EXT_buffer_storage GL_EXT_clip_control GL_EXT_color_buffer_half_float GL_EXT_compressed_ETC1_RGB8_sub_texture GL_EXT_copy_image GL_EXT_debug_label GL_EXT_debug_marker GL_EXT_discard_framebuffer GL_EXT_disjoint_timer_query GL_EXT_draw_buffers GL_EXT_draw_elements_base_vertex GL_EXT_float_blend GL_EXT_frag_depth GL_EXT_instanced_arrays GL_EXT_map_buffer_range GL_EXT_memory_object GL_EXT_memory_object_fd GL_EXT_multi_draw_indirect GL_EXT_multisampled_render_to_texture GL_EXT_multisampled_render_to_texture2 GL_EXT_occlusion_query_boolean GL_EXT_read_format_bgra GL_EXT_robustness GL_EXT_sRGB GL_EXT_sRGB_write_control GL_EXT_semaphore GL_EXT_semaphore_fd GL_EXT_separate_shader_objects GL_EXT_shader_non_constant_global_initializers GL_EXT_shader_texture_lod GL_EXT_shadow_samplers GL_EXT_texture_border_clamp GL_EXT_texture_compression_bptc GL_EXT_texture_compression_dxt1 GL_EXT_texture_compression_rgtc GL_EXT_texture_compression_s3tc_srgb GL_EXT_texture_filter_anisotropic GL_EXT_texture_format_BGRA8888 GL_EXT_texture_rg GL_EXT_texture_sRGB_decode GL_EXT_texture_storage GL_EXT_texture_type_2_10_10_10_REV GL_EXT_unpack_subimage GL_KHR_blend_equation_advanced GL_KHR_debug GL_NV_depth_buffer_float2 GL_NV_fence GL_NV_framebuffer_blit GL_NV_pack_subimage GL_NV_pixel_buffer_object GL_NV_read_depth GL_NV_read_stencil GL_OES_EGL_image GL_OES_EGL_image_external GL_OES_EGL_sync GL_OES_compressed_EAC_R11_signed_texture GL_OES_compressed_EAC_R11_unsigned_texture GL_OES_compressed_EAC_RG11_signed_texture GL_OES_compressed_EAC_RG11_unsigned_texture GL_OES_compressed_ETC1_RGB8_texture GL_OES_compressed_ETC2_RGB8_texture GL_OES_compressed_ETC2_RGBA8_texture GL_OES_compressed_ETC2_punchthroughA_RGBA8_texture GL_OES_compressed_ETC2_punchthroughA_sRGB8_alpha_texture GL_OES_compressed_ETC2_sRGB8_alpha8_texture GL_OES_compressed_ETC2_sRGB8_texture GL_OES_depth24 GL_OES_depth32 GL_OES_depth_texture GL_OES_depth_texture_cube_map GL_OES_draw_elements_base_vertex GL_OES_element_index_uint GL_OES_fbo_render_mipmap GL_OES_get_program_binary GL_OES_mapbuffer GL_OES_packed_depth_stencil GL_OES_primitive_bounding_box GL_OES_rgb8_rgba8 GL_OES_sample_shading GL_OES_standard_derivatives GL_OES_surfaceless_context GL_OES_texture_3D GL_OES_texture_border_clamp GL_OES_texture_float GL_OES_texture_float_linear GL_OES_texture_half_float GL_OES_texture_half_float_linear GL_OES_texture_npot GL_OES_texture_stencil8 GL_OES_vertex_array_object GL_OES_vertex_half_float
Disabled Extensions	GL_KHR_blend_equation_advanced GL_KHR_blend_equation_advanced_coherent
Disabled WebGL Extensions	
Window system binding vendor	Google Inc. (NVIDIA)
Window system binding version	1.5 (ANGLE 2.1.0 git hash: unknown hash)
Window system binding extensions	EGL_EXT_create_context_robustness EGL_ANGLE_surface_orientation EGL_KHR_create_context EGL_KHR_image EGL_KHR_image_base EGL_EXT_image_gl_colorspace EGL_KHR_gl_colorspace EGL_KHR_gl_texture_2D_image EGL_KHR_gl_texture_cubemap_image EGL_KHR_gl_renderbuffer_image EGL_KHR_get_all_proc_addresses EGL_KHR_fence_sync EGL_KHR_wait_sync EGL_ANGLE_create_context_webgl_compatibility EGL_CHROMIUM_create_context_bind_generates_resource EGL_KHR_swap_buffers_with_damage EGL_EXT_pixel_format_float EGL_KHR_surfaceless_context EGL_ANGLE_display_texture_share_group EGL_ANGLE_display_semaphore_share_group EGL_ANGLE_create_context_client_arrays EGL_ANGLE_program_cache_control EGL_ANGLE_robust_resource_initialization EGL_ANGLE_create_context_extensions_enabled EGL_ANDROID_blob_cache EGL_ANDROID_recordable EGL_ANGLE_create_context_backwards_compatible EGL_KHR_no_config_context EGL_IMG_context_priority EGL_KHR_create_context_no_error EGL_KHR_reusable_sync EGL_EXT_buffer_age EGL_KHR_mutable_render_buffer EGL_ANGLE_create_surface_swap_interval EGL_ANGLE_vulkan_image
XDG_CURRENT_DESKTOP	i3
XDG_SESSION_TYPE	x11
GDMSESSION	i3
Ozone platform	x11
Direct rendering version	unknown
Reset notification strategy	0x8252
GPU process crash count	14
gfx::BufferFormats supported for allocation and texturing	R_8: not supported, R_16: not supported, RG_88: not supported, RG_1616: not supported, BGR_565: not supported, RGBA_4444: not supported, RGBX_8888: not supported, RGBA_8888: not supported, BGRX_8888: not supported, BGRA_1010102: not supported, RGBA_1010102: not supported, BGRA_8888: not supported, RGBA_F16: not supported, YVU_420: not supported, YUV_420_BIPLANAR: not supported, P010: not supported
Compositor Information
Tile Update Mode	One-copy
Partial Raster	Enabled
GpuMemoryBuffers Status
R_8	Software only
R_16	Software only
RG_88	Software only
RG_1616	Software only
BGR_565	Software only
RGBA_4444	Software only
RGBX_8888	Software only
RGBA_8888	Software only
BGRX_8888	Software only
BGRA_1010102	Software only
RGBA_1010102	Software only
BGRA_8888	Software only
RGBA_F16	Software only
YVU_420	Software only
YUV_420_BIPLANAR	Software only
P010	Software only
Display(s) Information
Info	Display[0] bounds=[2400,1200 1200x1920], workarea=[2400,1200 1200x1920], scale=1, rotation=270, panel_rotation=270 external.
Color space (all)	{primaries:BT709, transfer:SRGB, matrix:RGB, range:FULL}
Buffer format (all)	BGRA_8888
SDR white level in nits	100
HDR relative maximum luminance	1
Bits per color component	8
Bits per pixel	24
Refresh Rate in Hz	59
Info	Display[2] bounds=[840,0 1920x1200], workarea=[840,0 1920x1200], scale=1, rotation=0, panel_rotation=0 external.
Color space (all)	{primaries:BT709, transfer:SRGB, matrix:RGB, range:FULL}
Buffer format (all)	BGRA_8888
SDR white level in nits	100
HDR relative maximum luminance	1
Bits per color component	8
Bits per pixel	24
Refresh Rate in Hz	59
Info	Display[4] bounds=[0,1200 1200x1920], workarea=[0,1200 1200x1920], scale=1, rotation=270, panel_rotation=270 external.
Color space (all)	{primaries:BT709, transfer:SRGB, matrix:RGB, range:FULL}
Buffer format (all)	BGRA_8888
SDR white level in nits	100
HDR relative maximum luminance	1
Bits per color component	8
Bits per pixel	24
Refresh Rate in Hz	59
Info	Display[6] bounds=[1200,1200 1200x1920], workarea=[1200,1200 1200x1920], scale=1, rotation=270, panel_rotation=270 external.
Color space (all)	{primaries:BT709, transfer:SRGB, matrix:RGB, range:FULL}
Buffer format (all)	BGRA_8888
SDR white level in nits	100
HDR relative maximum luminance	1
Bits per color component	8
Bits per pixel	24
Refresh Rate in Hz	59
Video Acceleration Information
Encoding	
Vulkan Information
Device Performance Information
Log Messages
[357971:357971:0820/025540.090955:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/025540.091048:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/025540.091091:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/025540.091133:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/025540.091176:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/025540.091529:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/025540.091575:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/025540.091616:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/025540.091675:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/025540.091718:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/025540.092077:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/025540.092142:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/025540.092203:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/025540.092292:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/025540.092379:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/025542.561287:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/025542.561499:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/025542.561756:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/025542.561904:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/025542.562073:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/025542.565455:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/025542.565729:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/025542.565908:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/025542.566075:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/025542.566245:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/025542.569276:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/025542.569448:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/025542.569539:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/025542.569636:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/025542.569740:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/025542.572072:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/025542.572228:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/025542.572295:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/025542.572348:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/025542.572409:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/025542.574549:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/025542.574651:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/025542.574710:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/025542.574800:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/025542.574891:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/025542.576597:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/025542.576731:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/025542.576797:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/025542.576854:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/025542.576924:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/025542.961370:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/025542.961512:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/025542.961632:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/025542.961743:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/025542.961853:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/025542.962615:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/025542.962747:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/025542.962810:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/025542.962877:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/025542.962955:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/025542.963460:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/025542.963528:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/025542.963576:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/025542.963627:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/025542.963675:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/025542.964317:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/025542.964398:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/025542.964449:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/025542.964500:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/025542.964552:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/025542.964941:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/025542.965000:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/025542.965068:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/025542.965126:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/025542.965178:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/025542.965587:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/025542.965654:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/025542.965714:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/025542.965757:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/025542.965802:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/025906.679706:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/025906.679831:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/025906.679911:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/025906.679984:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/025906.680053:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/025906.726610:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/025906.726716:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/025906.726768:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/025906.726804:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/025906.726842:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/025906.727681:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/025906.727782:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/025906.727822:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/025906.727861:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/025906.727915:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/025906.733410:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/025906.733515:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/025906.733560:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/025906.733604:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/025906.733651:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/034626.137636:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/034626.137757:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/034626.137870:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/034626.137949:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/034626.138015:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/034626.182516:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/034626.182625:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/034626.182680:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/034626.182725:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/034626.182771:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/034626.183525:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/034626.183617:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/034626.183696:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/034626.183764:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/034626.183805:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/034626.187861:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/034626.188024:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/034626.188119:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/034626.188182:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/034626.188233:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/043343.101628:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/043343.101775:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/043343.101879:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/043343.101971:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/043343.102106:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/043343.149920:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/043343.150022:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/043343.150089:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/043343.150141:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/043343.150199:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/043343.151133:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/043343.151214:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/043343.151259:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/043343.151329:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/043343.151502:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/043343.157732:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/043343.157902:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/043343.158031:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/043343.158121:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/043343.158241:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/050331.507501:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/050331.507745:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/050331.507864:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/050331.508030:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/050331.508165:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/051901.601197:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/051901.601341:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/051901.601416:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/051901.601533:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/051901.601657:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/051901.631716:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/051901.631822:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/051901.631863:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/051901.631969:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/051901.632015:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/051901.834641:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/051901.834759:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/051901.834821:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/051901.834870:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/051901.834941:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/051901.835986:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/051901.836154:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/051901.836227:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/051901.836275:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/051901.836333:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/051901.836918:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/051901.837012:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/051901.837061:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/051901.837099:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/051901.837139:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/051901.840802:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/051901.840921:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/051901.840983:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/051901.841034:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/051901.841082:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/051901.841656:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/051901.841738:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/051901.841785:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/051901.841825:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/051901.841872:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/051901.842336:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/051901.842411:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/051901.842459:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/051901.842501:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/051901.842547:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/051907.217397:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/051907.217573:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/051907.217669:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/051907.217768:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/051907.217890:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/051907.218469:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/051907.218569:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/051907.218615:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/051907.218683:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/051907.218749:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/051907.219314:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/051907.219438:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/051907.219503:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/051907.219550:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/051907.219616:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/051907.222888:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/051907.223013:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/051907.223071:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/051907.223123:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/051907.223177:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/051907.223984:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/051907.224198:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/051907.224286:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/051907.224356:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/051907.224440:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/051907.226337:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/051907.226484:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/051907.226586:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/051907.226647:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/051907.226703:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/052028.527165:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/052028.527382:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/052028.527582:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/052028.527693:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/052028.527800:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/052028.528676:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/052028.529168:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/052028.529326:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/052028.529474:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/052028.529605:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/052028.530315:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/052028.530462:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/052028.530585:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/052028.530699:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/052028.530807:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/052028.531704:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/052028.531861:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/052028.531976:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/052028.532071:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/052028.532170:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/052028.532663:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/052028.532775:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/052028.532877:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/052028.532969:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/052028.533065:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/052028.533625:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/052028.533761:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/052028.533869:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/052028.533972:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/052028.534080:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/052028.536712:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/052028.536836:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/052028.536958:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/052028.537030:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/052028.537126:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/052028.539263:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/052028.539397:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/052028.539470:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/052028.539549:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/052028.539628:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/052028.540163:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/052028.540260:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/052028.540307:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/052028.540363:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/052028.540422:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/052028.541166:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/052028.541267:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/052028.541315:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/052028.541369:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/052028.541422:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/052028.541854:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/052028.541961:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/052028.542028:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/052028.542079:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/052028.542152:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/052028.542869:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/052028.542986:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/052028.543036:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/052028.543080:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/052028.543129:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/052028.924247:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/052028.924443:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/052028.924619:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/052028.924756:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/052028.924879:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/052028.925597:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/052028.925709:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/052028.925791:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/052028.925860:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/052028.925928:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/052301.659087:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/052301.659252:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/052301.659412:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/052301.659560:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/052301.659679:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/052301.701888:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/052301.701991:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/052301.702073:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/052301.702130:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/052301.702178:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/052301.703268:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/052301.703400:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/052301.703468:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/052301.703528:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/052301.703592:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/052301.712645:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/052301.712769:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/052301.712824:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/052301.712888:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/052301.712948:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/055025.420674:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/055025.420851:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/055025.420955:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/055025.421086:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/055025.421166:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/055025.421852:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/055025.422032:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/055025.422133:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/055025.422223:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/055025.422317:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/055025.436189:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/055025.436333:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/055025.436432:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/055025.436502:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/055025.436573:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/055025.441388:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/055025.441541:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/055025.441617:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/055025.441665:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/055025.441731:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/055025.442272:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/055025.442379:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/055025.442451:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/055025.442499:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/055025.442553:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/055025.443146:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/055025.443263:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/055025.443326:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/055025.443389:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/055025.443464:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/055025.446190:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/055025.446309:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/055025.446368:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/055025.446440:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/055025.446514:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/055025.447057:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/055025.447195:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/055025.447273:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/055025.447351:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/055025.447427:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/055025.448069:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/055025.448224:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/055025.448284:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/055025.448331:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/055025.448390:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/055025.449242:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/055025.449371:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/055025.449428:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/055025.449481:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/055025.449547:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/055025.449920:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/055025.449971:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/055025.450012:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/055025.450052:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/055025.450090:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/055025.450506:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/055025.450559:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/055025.450597:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/055025.450632:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/055025.450669:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/055025.684394:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/055025.684512:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/055025.684558:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/055025.684610:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/055025.684663:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/055025.684990:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/055025.685043:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/055025.685078:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/055025.685116:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/055025.685220:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/060944.929591:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/060944.929717:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/060944.929799:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/060944.929858:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/060944.929911:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/060944.986427:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/060944.986541:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/060944.986589:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/060944.986627:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/060944.986670:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/060944.988802:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/060944.988898:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/060944.988942:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/060944.988984:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/060944.989031:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/060944.989411:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/060944.989470:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/060944.989513:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/060944.989575:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/060944.989618:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101845.647499:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101845.647641:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101845.648522:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101845.648815:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101845.649170:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101845.649823:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101845.649933:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101845.650032:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101845.650124:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101845.650220:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101845.650919:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101845.651037:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101845.651152:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101845.651247:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101845.651352:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101845.654928:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101845.655040:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101845.655144:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101845.655200:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101845.655244:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101845.655667:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101845.655734:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101845.655778:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101845.655818:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101845.655862:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101845.656240:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101845.656299:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101845.656338:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101845.656375:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101845.656410:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101845.660206:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101845.660539:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101845.660663:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101845.660719:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101845.660804:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101845.661706:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101845.661879:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101845.661957:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101845.662049:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101845.662130:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101845.663119:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101845.663273:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101845.663337:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101845.663404:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101845.663489:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101845.664403:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101845.664519:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101845.664566:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101845.664610:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101845.664659:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101845.665309:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101845.665424:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101845.665526:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101845.665590:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101845.665673:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101845.666606:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101845.666727:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101845.666791:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101845.666905:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101845.666967:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.643467:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.643662:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.643778:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.643967:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.644142:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.644834:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.644951:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.645037:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.645118:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.645202:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.645893:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.646005:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.646089:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.646176:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.646264:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.647561:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.647733:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.647807:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.647929:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.648017:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.648642:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.648733:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.648808:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.648888:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.648972:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.649655:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.649751:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.649825:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.649909:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.649990:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.653846:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.653990:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.654074:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.654157:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.654241:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.654806:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.654944:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.655036:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.655117:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.655199:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.655806:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.655940:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.656029:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.656112:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.656201:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.657245:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.657369:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.657456:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.657533:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.657620:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.658147:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.658259:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.658339:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.658415:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.658496:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.659084:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.659191:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.659271:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.659370:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.659453:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.845718:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.845864:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.845934:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.845989:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.846072:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.846662:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.846733:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.846778:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.846827:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.846881:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.847431:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.847495:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.847537:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.847577:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.847617:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.848361:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.848442:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.848485:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.848527:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.848569:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.848991:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.849057:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.849100:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.849140:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.849181:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.849724:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.849792:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.849840:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.849883:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.849924:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.857287:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.857391:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.857438:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.857475:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.857529:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.858053:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.858152:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.858256:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.858322:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.858388:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.859603:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.859748:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.859810:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.859869:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.859949:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.860961:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.861044:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.861105:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.861161:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.861222:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.861667:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.861735:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.861793:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.861848:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.861915:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.862416:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.862481:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.862539:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.862610:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.862673:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.865966:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.866083:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.866160:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.866221:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.866282:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.866786:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.866863:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.866924:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.866978:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.867037:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.867580:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.867680:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.867735:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.867785:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.867839:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.868672:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.868781:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.868838:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.868878:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.868922:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.869353:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.869440:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.869500:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.869671:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.869727:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.870208:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.870265:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.870313:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.870355:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.870398:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.872563:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.872664:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.872715:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.872752:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.872795:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.873257:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.873324:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.873372:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.873428:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.873474:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.873906:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.873984:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.874026:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.874065:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.874107:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.874696:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.874784:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.874847:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.874896:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.874940:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.875327:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.875387:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.875426:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.875466:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.875512:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/101945.876023:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/101945.876092:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/101945.876133:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/101945.876173:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/101945.876213:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181641.424851:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181641.425005:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181641.425088:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181641.425157:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181641.425224:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181641.455774:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181641.455923:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181641.456000:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181641.456047:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181641.456101:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181641.558295:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181641.558405:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181641.558456:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181641.558496:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181641.558542:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181641.561975:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181641.562105:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181641.562163:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181641.562218:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181641.562285:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181641.563074:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181641.563191:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181641.563240:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181641.563285:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181641.563338:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181641.566288:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181641.566385:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181641.566431:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181641.566478:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181641.566524:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181641.566861:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181641.566930:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181641.567003:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181641.567054:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181641.567109:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181641.568482:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181641.568603:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181641.568643:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181641.568681:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181641.568732:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181641.629876:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181641.630014:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181641.630085:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181641.630158:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181641.630241:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181641.676407:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181641.676518:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181641.676581:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181641.676629:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181641.676680:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181641.677492:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181641.677582:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181641.677644:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181641.677691:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181641.677740:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181641.702689:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181641.702794:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181641.702855:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181641.702905:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181641.702956:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181641.788498:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181641.788626:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181641.788683:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181641.788733:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181641.788802:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181641.813504:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181641.813643:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181641.813727:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181641.813787:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181641.813861:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181641.876498:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181641.876623:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181641.876675:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181641.876725:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181641.876780:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181641.877464:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181641.877544:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181641.877594:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181641.877671:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181641.877723:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181641.989686:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181641.989812:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181641.989867:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181641.989912:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181641.989964:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181641.990578:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181641.990647:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181641.990695:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181641.990751:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181641.990795:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181642.011326:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181642.011442:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181642.011502:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181642.011555:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181642.011613:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181642.012176:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181642.012261:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181642.012317:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181642.012372:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181642.012440:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181642.012833:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181642.012898:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181642.012949:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181642.013001:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181642.013054:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181642.013607:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181642.013694:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181642.013765:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181642.013815:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181642.013870:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181642.014190:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181642.014254:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181642.014309:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181642.014366:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181642.014413:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181642.014829:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181642.014893:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181642.014938:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181642.014982:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181642.015027:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181642.957180:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181642.957369:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181642.957544:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181642.957671:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181642.957791:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181642.958934:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181642.959053:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181642.959144:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181642.959244:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181642.959347:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181643.773540:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181643.773674:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181643.773754:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181643.773806:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181643.773870:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181643.774651:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181643.774795:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181643.774867:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181643.774925:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181643.774983:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181643.775809:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181643.775940:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181643.776009:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181643.776056:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181643.776104:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181643.778019:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181643.778130:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181643.778196:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181643.778242:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181643.778286:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181643.778838:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181643.778947:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181643.779007:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181643.779054:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181643.779108:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181643.779860:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181643.779983:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181643.780045:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181643.780087:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181643.780131:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181643.783690:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181643.783824:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181643.783877:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181643.783940:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181643.783991:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181643.784606:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181643.784719:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181643.784773:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181643.784818:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181643.784863:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181643.785603:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181643.785730:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181643.785808:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181643.785914:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181643.786045:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181643.787038:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181643.787172:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181643.787250:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181643.787324:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181643.787434:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181643.787970:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181643.788067:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181643.788142:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181643.788209:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181643.788278:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181643.788902:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181643.789000:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181643.789069:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181643.789134:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181643.789203:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181656.111246:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181656.111463:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181656.111601:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181656.111692:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181656.111847:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181656.139778:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181656.139906:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181656.139949:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181656.139997:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181656.140042:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181656.288572:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181656.288732:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181656.288806:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181656.288850:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181656.288905:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181656.289432:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181656.289569:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181656.289668:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181656.289758:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181656.289851:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181656.290479:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181656.290613:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181656.290699:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181656.290774:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181656.290895:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181656.292371:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181656.292525:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181656.292659:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181656.292769:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181656.292870:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181656.293874:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181656.294013:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181656.294105:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181656.294218:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181656.294307:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181656.297403:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181656.297528:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181656.297602:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181656.297651:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181656.297699:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181656.614733:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181656.614856:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181656.614921:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181656.615000:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181656.615080:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181656.615578:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181656.615689:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181656.615771:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181656.615836:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181656.615925:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181656.616581:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181656.616690:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181656.616809:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181656.616904:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181656.617003:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181656.618038:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181656.618190:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181656.618263:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181656.618311:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181656.618354:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181656.618891:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181656.618989:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181656.619041:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181656.619112:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181656.619173:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/181656.619731:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/181656.619809:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/181656.619858:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/181656.619912:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/181656.619957:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/200645.801839:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/200645.801997:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/200645.802063:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/200645.802138:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/200645.802221:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/200645.830360:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/200645.830481:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/200645.830547:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/200645.830599:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/200645.830662:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/200645.986353:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/200645.986519:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/200645.986611:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/200645.986682:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/200645.986745:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/200645.990711:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/200645.990966:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/200645.991069:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/200645.991143:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/200645.991275:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/200645.993773:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/200645.994012:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/200645.994098:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/200645.994171:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/200645.994252:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/200645.999359:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/200645.999579:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/200645.999703:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/200645.999792:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/200645.999886:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/200646.003967:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/200646.008115:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/200646.008213:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/200646.008289:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/200646.008402:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/200646.009820:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/200646.009932:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/200646.009994:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/200646.010063:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/200646.010136:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/200646.233583:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/200646.233756:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/200646.233856:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/200646.233926:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/200646.233995:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/200646.234872:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/200646.234992:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/200646.235051:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/200646.235114:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/200646.235160:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/200646.235787:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/200646.235993:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/200646.236054:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/200646.236114:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/200646.236189:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/200646.237044:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/200646.237128:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/200646.237169:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/200646.237207:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/200646.237261:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/200646.238781:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/200646.238888:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/200646.238950:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/200646.239001:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/200646.239066:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/200646.244036:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/200646.244133:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/200646.244210:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/200646.244264:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/200646.244311:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/200655.709486:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/200655.709743:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/200655.709951:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/200655.710201:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/200655.710505:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/200655.738365:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/200655.738489:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/200655.738553:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/200655.738593:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/200655.738648:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/200655.761499:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/200655.761634:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/200655.761690:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/200655.761749:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/200655.761808:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/200655.762908:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/200655.763007:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/200655.763055:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/200655.763099:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/200655.763155:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/200655.766118:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/200655.766236:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/200655.766297:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/200655.766360:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/200655.766421:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/200655.767438:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/200655.767537:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/200655.767582:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/200655.767622:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/200655.767660:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/200655.768027:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/200655.768075:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/200655.768115:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/200655.768149:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/200655.768186:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
[357971:357971:0820/200655.768584:ERROR:gl_surface_egl.cc(837)] : EGL Driver message (Critical) eglCreateContext: display had a context loss
[357971:357971:0820/200655.768632:ERROR:gl_context_egl.cc(352)] : eglCreateContext failed with error EGL_SUCCESS
[357971:357971:0820/200655.768669:ERROR:gpu_channel_manager.cc(831)] : ContextResult::kFatalFailure: Failed to create shared context for virtualization.
[357971:357971:0820/200655.768707:ERROR:shared_image_stub.cc(537)] : SharedImageStub: unable to create context
[357971:357971:0820/200655.768762:ERROR:gpu_channel.cc(572)] : GpuChannel: Failed to create SharedImageStub
```
